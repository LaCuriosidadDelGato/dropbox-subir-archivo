#!/usr/bin/env python
# -*- coding: utf-8 -*-

# crear la app y el token en: https://www.dropbox.com/developers/apps/create

# pip install Dropbox
import dropbox

def subir(archivo,donde):
    global TOKEN
    
    try:
        dbx = dropbox.Dropbox(TOKEN)
    except:
        return 'Error al conectar el token'
    
    try:
        dbx.files_upload(open(archivo, 'rb').read(), donde, dropbox.files.WriteMode('overwrite', None))
    except:
        return 'Error al subir el archivo'

    return 'Archivo subido'

archivoASubir = 'prueba.txt'
dondeSubir = '/pythonPrueba/prueba.txt'
TOKEN = 'eltokendelaapp'

res = subir(archivoASubir,dondeSubir)
print(res)